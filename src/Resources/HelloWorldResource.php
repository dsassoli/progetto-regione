<?php

namespace Resources;

use Tonic\Response, Tonic\Resource;

/**
 * @uri /
*/
class HelloWorldResource extends Resource {

	/**
	 * @method GET
	 */
	function get() {

		$body = "<html><body><p>Hello</p></body></html>";

		return new Response(Response::OK, $body, array(
				'content-type' => 'text/html'
		));
	}
}

?>