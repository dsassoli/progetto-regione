<?php

// Carica tutte le classi necessarie a Tonic
require_once 'src/Tonic/Autoloader.php';

// Tonic option
$opt = array(
		'load' => 'src/Resources/*.php'
);

// Inizializzo l'applicazione indicando dove sono le risorse
$app = new Tonic\Application($opt);

// recupero la request HTTP
$request = new Tonic\Request();


//decode JSON data received from HTTP request
if ($request->contentType == 'application/json') {
	$request->data = json_decode($request->data);
}

// gestisco l'errore nel caso la risorsa non esista
try {
	$resource = $app->getResource($request);
} catch(Tonic\NotFoundException $e) {
	$resource = new Resources\NotFoundResource($app, $request, array());
}

// gestisco l'errore nel caso esploda il mio controller
try {
	$response = $resource->exec();
} catch(Tonic\Exception $e) {
	$resource = new Resources\FatalErrorResource($app, $request, array());
	$response = $resource->exec();
}

// encode output
if ($response->contentType == 'application/json') {
	$response->body = json_encode($response->body);
}
$response->output();
?>