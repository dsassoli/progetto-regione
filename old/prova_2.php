<em><html>
<head>
<title>Streaming Regione Toscana</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
</head>

<body>
<?php 
	include_once 'include/crea-xml.php';
?>
 
   <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
<div class="row-fluid">
  <div class="span12">

	 <form class="form-search">   
	 <button id="1" class="btn btn-large btn-danger" type="button"><i class="icon-facetime-video"></i>  Diretta  </button>   
     <button id="2" class="btn btn-large" type="button"><i class="icon-eye-open"></i> I pi� visti</button>   
	 <button id="3" class="btn btn-large" type="button"><i class="icon-search"></i>  I pi� ricercati</button>   	
	 <button id="4" class="btn btn-large" type="button"><i class="icon-thumbs-up"></i> Di tendenza</button>   
	 <input type="text" class="search-query" placeholder="Titolo o argomento">
	 <button type="submit" class="btn">Cerca</button>
	 </form>
</div>
</div>
	 
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12">
<script> $('#1').on('click', function (e)   {  loadPlayList('xml.xml'); }) </script>
	<script> $('#2').on('click', function (e)   {  loadPlayList('playlist.xml'); }) </script>	

<script type='text/javascript' src='jwplayer_5.js'></script>
<div id='mediaplayer'></div>

  <script type="text/javascript">
  jwplayer('mediaplayer').setup({
    'flashplayer': 'player_5.swf',
    'id': 'playerID',
    'width': '950',
    'height': '640',
    'playlistfile': 'xml.xml',
    'playlist.position': 'right',
    'playlist.size': '250',
    'controlbar': 'over',
    'repeat': 'list',
	'plugins': 
		{
       'infobox-1': {},
	   'fbit-1': {'link': 'www.consiglio.regione.toscana.it'},
	   'plusone-1': {},
	   'tweetit-1': {'link': 'www.consiglio.regione.toscana.it'},
	    }
  });
  function loadPlayList(pathPlayList)
   {
       jwplayer().load(pathPlayList);
   }	
</script>
    </div>
  </div>
</div>

</html>
</em>
