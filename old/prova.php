<em><html>
<head>
<title>Streaming Regione Toscana</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
</head>

<body>
<?php
$conf_host = 'localhost';
$conf_user = 'root';
$conf_pass = 'root';
$conf_db = 'rprovaprova';
// connessione a MySQL
$link = mysql_connect($conf_host, $conf_user, $conf_pass);
if (!$link) {
   die('Errore nella connessione: <br>' . mysql_error());
}
else{
echo("Collegato <br>");
}
// selezione del database
$db_selected = mysql_select_db($conf_db, $link);
if (!$db_selected) {
   die ('Errore nella selezione del DB: <br>' . mysql_error());
}
else{
echo("Scelto db <br>");
}
// invio query
$query = "SELECT evento.descrizione, canali.streamer,canali.content, canali.provider
FROM referente, users, evento,canali,servizio
WHERE canali.id=evento.idcanale 
and evento.userid=users.id
and servizio.referente_id=referente.id
and referente.id=users.id
and servizio.nome='streaming'
and evento.orainizio <=(SELECT CURTIME())
and evento.orafine >=(SELECT CURTIME())
and evento.datainizio >=(SELECT CURDATE())
and evento.datafine <=(SELECT CURDATE());";

$result = mysql_query($query, $link);
if (!$result) {
   die('Errore nella query: ' . mysql_error());
}

$xml_output =(  
"<rss version=\"2.0\" xmlns:media=\"http://search.yahoo.com/mrss/\" xmlns:jwplayer=\"http://developer.longtailvideo.com/trac/\"> 
<channel>
  <title>PlayList Streaming Regione Toscana</title>");

while ($riga = mysql_fetch_assoc($result))
{
$xml_output .=("
<item> 
    <title>$riga[descrizione]</title> 
    <description> $riga[descrizione]</description>
    <media:content url=\"$riga[content]\" /> 
    <media:thumbnail url=\"logo_2.jpg\" /> 
    <jwplayer:provider>$riga[provider]</jwplayer:provider> 
    <jwplayer:streamer>$riga[streamer]</jwplayer:streamer> 
    <jwplayer:duration>00.00</jwplayer:duration> 
  </item> ");        
}              
$xml_output .=("   			  		 
 </channel>
 </rss>");

$filename = 'xml.xml';
// Let's make sure the file exists and is writable first.
if (is_writable($filename)) {
    // In our example we're opening $filename in append mode.
    // The file pointer is at?????????<?ph the bottom of the file hence
    // that's where $somecontent will go when we fwrite() it.
    if (!$handle = fopen($filename, 'w')) {
         echo "Cannot open file ($filename)";
         exit;
    }
    // Write $somecontent to our opened file.
    if (fwrite($handle, $xml_output) === FALSE) {
        echo "Cannot write to file ($filename)";
        exit;
    }
    echo "Success, wrote to file ($filename)";
    fclose($handle);

} else {
    echo "The file $filename is not writable";
}
?>
 
   <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
<div class="row-fluid">
  <div class="span12">

	 <form class="form-search">   
	 <button id="1" class="btn btn-large btn-danger" type="button"><i class="icon-facetime-video"></i>  Diretta  </button>   
     <button id="2" class="btn btn-large" type="button"><i class="icon-eye-open"></i> I pi� visti</button>   
	 <button id="3" class="btn btn-large" type="button"><i class="icon-search"></i>  I pi� ricercati</button>   	
	 <button id="4" class="btn btn-large" type="button"><i class="icon-thumbs-up"></i> Di tendenza</button>   
	 <input type="text" class="search-query" placeholder="Titolo o argomento">
	 <button type="submit" class="btn">Cerca</button>
	 </form>
</div>
</div>
	 
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12">
<script> $('#1').on('click', function (e)   {  loadPlayList('xml.xml'); }) </script>
	<script> $('#2').on('click', function (e)   {  loadPlayList('playlist.xml'); }) </script>	

<script type='text/javascript' src='jwplayer_5.js'></script>
<div id='mediaplayer'></div>

  <script type="text/javascript">
  jwplayer('mediaplayer').setup({
    'flashplayer': 'player_5.swf',
    'id': 'playerID',
    'width': '950',
    'height': '640',
    'playlistfile': 'xml.xml',
    'playlist.position': 'right',
    'playlist.size': '250',
    'controlbar': 'over',
    'repeat': 'list',
	'plugins': 
		{
       'infobox-1': {},
	   'fbit-1': {'link': 'www.consiglio.regione.toscana.it'},
	   'plusone-1': {},
	   'tweetit-1': {'link': 'www.consiglio.regione.toscana.it'},
	    }
  });
  function loadPlayList(pathPlayList)
   {
       jwplayer().load(pathPlayList);
   }	
</script>
    </div>
  </div>
</div>

</html>
</em>